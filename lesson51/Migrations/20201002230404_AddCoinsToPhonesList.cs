﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace lesson51.Migrations
{
    public partial class AddCoinsToPhonesList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Phones_Companies_Companyid",
                table: "Phones");

            migrationBuilder.DropForeignKey(
                name: "FK_Phones_Companies_CompanId",
                table: "Phones");

            migrationBuilder.DropIndex(
                name: "IX_Phones_Companyid",
                table: "Phones");

            migrationBuilder.DropColumn(
                name: "Companyid",
                table: "Phones");

            migrationBuilder.DropColumn(
                name: "Age",
                table: "Phones");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                table: "Phones",
                newName: "Companyid");

            migrationBuilder.RenameIndex(
                name: "IX_Phones_CompanyId",
                table: "Phones",
                newName: "IX_Phones_Companyid");

            migrationBuilder.AlterColumn<int>(
                name: "Companyid",
                table: "Phones",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Age = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Companies",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_CompanyId",
                table: "Users",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Phones_Companies_Companyid",
                table: "Phones",
                column: "Companyid",
                principalTable: "Companies",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Phones_Companies_Companyid",
                table: "Phones");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.RenameColumn(
                name: "Companyid",
                table: "Phones",
                newName: "CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Phones_Companyid",
                table: "Phones",
                newName: "IX_Phones_CompanyId");

            migrationBuilder.AlterColumn<int>(
                name: "CompanyId",
                table: "Phones",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Companyid",
                table: "Phones",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Age",
                table: "Phones",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Phones_Companyid",
                table: "Phones",
                column: "Companyid");

            migrationBuilder.AddForeignKey(
                name: "FK_Phones_Companies_Companyid",
                table: "Phones",
                column: "Companyid",
                principalTable: "Companies",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Phones_Companies_CompanyId",
                table: "Phones",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
