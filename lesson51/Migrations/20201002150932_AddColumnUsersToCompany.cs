﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace lesson51.Migrations
{
    public partial class AddColumnUsersToCompany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
               name: "Users",
               table: "Companies",
               nullable: false,
                defaultValue: 0
              );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
               name: "Users",
               table: "Companies");
        }
    }
}
