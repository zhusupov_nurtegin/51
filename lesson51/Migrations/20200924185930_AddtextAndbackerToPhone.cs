﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace lesson51.Migrations
{
    public partial class AddtextAndbackerToPhone : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "backer",
                table: "Phones",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "text",
                table: "Phones",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "WebSite",
                table: "Companies",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Companies",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "backer",
                table: "Phones");

            migrationBuilder.DropColumn(
                name: "text",
                table: "Phones");

            migrationBuilder.AlterColumn<string>(
                name: "WebSite",
                table: "Companies",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Companies",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
