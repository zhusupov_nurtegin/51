﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace lesson51.Migrations
{
    public partial class AddNewCompaniesToCompanies : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(table: "Companies", columns: new[] { "Name", "Website" }, values: new[] { "Mi", "mi.com" });
            migrationBuilder.InsertData(table: "Companies", columns: new[] { "Name", "Website" }, values: new[] { "Motorolla", "motorolla.com" });
            migrationBuilder.InsertData(table: "Companies", columns: new[] { "Name", "Website" }, values: new[] { "Nokia", "nokia.com" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
