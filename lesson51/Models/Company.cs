﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace lesson51.Models
{
    public class Company
    {
        public int id { get; set; }

        [Required(ErrorMessage = "Укажите название компании")]
        [Remote(action: "CheckName", controller: "Companies", ErrorMessage = "Это название компании уже занято")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Укажите веб-сайт компании")]
        [RegularExpression(@"[A-Za-z0-9.-]+\.[A-Za-z]", ErrorMessage = "Некорректный адрес веб-сайта")]
        public string WebSite { get; set; }

        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный адрес электронной почты")]
        public string Email { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FoundDate { get; set; }

        public List<User> Users { get; set; }

        public Company()

        {

            Users = new List<User>();

        }
    }
}
