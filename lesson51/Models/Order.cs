﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace lesson51.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        [Required(ErrorMessage = "Не указано имя")]

        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина имени должна быть от 2 до 50 символов")]

        public string Name { get; set; }
        public string User { get; set; }

        public string Address { get; set; }

        public string ContactPhone { get; set; }



        public int PhoneId { get; set; }

        public Phone Phone { get; set; }
    }
}
