﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using lesson51.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace lesson51.Controllers
{
    public class OrdersController : Controller
    {
        private readonly PhoneShopContext _context;

        public OrdersController(PhoneShopContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View(_context.Orders.ToList());
        }
        public IActionResult Create()
        {
            ViewBag.Phones = new SelectList(_context.Phones, "Id", "Name");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OrderId,User,Address,ContactPhone,PhoneId")] Order order)
        {
            if (ModelState.IsValid)
            {
              await  _context.AddAsync(order);
               await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Phones = new SelectList(_context.Phones, "Id", "Name");
            return View(order);
        }
    }
}
