﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

using lesson51.Models;
using lesson51.ViewModels;

namespace lesson51.Controllers
{ 
      public enum SortInPhones
{
    NameAsc,

    NameDesc,
    CompanyAsc,

    CompanyDesc,

    PriceAsc, 
    PriceDesc,

    PointAsc,
    PointDesc
}

    public class PhonesController : Controller
    {

        private readonly PhoneShopContext _context;
        //private readonly IHostingEnvironment appEnvironment;
        public PhonesController(PhoneShopContext context)
        {
            _context = context;
            

        }

        // GET: Phones
        public async Task<IActionResult> Index(int? company, string name, int price,int points, SortInPhones sortPhones = SortInPhones.NameAsc, int page = 1)
        {
            IQueryable<Phone> phones = _context.Phones.Include(u => u.Company);

            if (company != null && company != 0)
            {
                phones = phones.Where(p => p.CompanId == company);
            }

            if (!String.IsNullOrEmpty(name))
            {
                phones = phones.Where(p => p.Name.Contains(name));

            }
            if (price != null && price != 0)
            {
                phones = phones.Where(p => p.Price == price);
            }
            if (points != null && points != 0)
            {
                phones = phones.Where(p => p.points == points);
            }

            ViewBag.NameSort = sortPhones == SortInPhones.NameAsc ? SortInPhones.NameDesc : SortInPhones.NameAsc;
            ViewBag.CompSort = sortPhones == SortInPhones.CompanyAsc ? SortInPhones.CompanyDesc : SortInPhones.CompanyAsc;
            ViewBag.PriceSort = sortPhones == SortInPhones.PriceAsc ? SortInPhones.PriceDesc : SortInPhones.PriceAsc;
            ViewBag.PointSort = sortPhones == SortInPhones.PointAsc ? SortInPhones.PointDesc : SortInPhones.PointAsc;
            switch (sortPhones)

            {

                case SortInPhones.NameDesc:

                    phones = phones.OrderByDescending(s => s.Name);

                    break;

                case SortInPhones.CompanyAsc:

                    phones = phones.OrderBy(s => s.Company.Name);

                    break;

                case SortInPhones.CompanyDesc:

                    phones = phones.OrderByDescending(s => s.Company.Name);

                    break;
                case SortInPhones.PriceAsc:

                    phones = phones.OrderBy(s => s.Price);

                    break;

                case SortInPhones.PriceDesc:

                    phones = phones.OrderByDescending(s => s.Price);

                    break;
                case SortInPhones.PointAsc:

                    phones = phones.OrderBy(s => s.Price);

                    break;

                case SortInPhones.PointDesc:

                    phones = phones.OrderByDescending(s => s.Price);

                    break;

                default:

                    phones = phones.OrderBy(s => s.Name);

                    break;

            }
            int pageSize = 5;
            var count = await phones.CountAsync();
            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            List<Company> companies = _context.Companies.ToList();
            companies.Insert(0, new Company { Name = "Все", id = 0});

            PhoneViewModel viewModel = new PhoneViewModel
            {
                phones = await phones.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync(),
                PageViewModel = pageViewModel,
                Phones = new SelectList(companies, "id", "Name", "Price"),
                Name = name,
                Price = price
            };



            return View(viewModel);
        }

        // GET: Phones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var phone = await _context.Phones
                .Include(p => p.Company)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (phone == null)
            {
                return NotFound();
            }

            return View(phone);
        }

        // GET: Phones/Create

        public IActionResult Create()
        {
            ViewData["Companies"] = new SelectList(_context.Companies, "id", "Name");
            return View();
        }
        // POST: Phones/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Company,Price, CompanId")] Phone phone)
        {
        
            if (ModelState.IsValid)
            {
                _context.Add(phone);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Companies"] = new System.Web.Mvc.SelectList(_context.Companies, "id", "Name");
            return View(phone);
        }
        public IActionResult MakeReview()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> MakeReview([Bind("points, text,backer")]Phone phone) {

            if (ModelState.IsValid)
            {
               await _context.AddRangeAsync(phone.points, phone.text, phone.backer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
           
            return View(phone);

        }

        public IActionResult CreateCompany()
        {
            return View("Views/Companies/CreateCompany.cshtml");
        }
  
        // GET: Phones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var phone = await _context.Phones.FindAsync(id);
            if (phone == null)
            {
                return NotFound();
            }
            return View(phone);
        }

        // POST: Phones/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Company,Price, CompanId")] Phone phone)
        {
            if (id != phone.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(phone);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PhoneExists(phone.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(phone);
        }

      /*  public async Task<IActionResult> Download(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var phone = await _context.Phones
                .FirstOrDefaultAsync(m => m.Id == id);
            if (phone == null)
            {
                return NotFound();
            }


            return View(phone);
        }*/

       /* public IActionResult ConfirmedDownload(int? id)
        {
            string filePath = Path.Combine(appEnvironment.ContentRootPath, "MyFiles/Phones.txt");
            string fileType = "application/text";
            string fileName = "Phones.txt";

            return PhysicalFile(filePath, fileType, fileName);
        }*/
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var phone = await _context.Phones
                .FirstOrDefaultAsync(m => m.Id == id);
            if (phone == null)
            {
                return NotFound();
            }

            return View(phone);
        }



        // POST: Phones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var phone = await _context.Phones.FindAsync(id);
            _context.Phones.Remove(phone);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PhoneExists(int id)
        {
            return _context.Phones.Any(e => e.Id == id);
        }
       /* public IActionResult Thanks()
        {
            ViewBag.Points = 10;
            return View();
        }*/
    }
}
