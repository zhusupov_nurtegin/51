﻿using lesson51.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lesson51
{ 
    public class PhoneShopContext : DbContext
    {  
        public DbSet<Phone> Phones { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<User> Users { get; set; }

        public PhoneShopContext(DbContextOptions<PhoneShopContext> options)

            : base(options)

        {

        }
    }
}
