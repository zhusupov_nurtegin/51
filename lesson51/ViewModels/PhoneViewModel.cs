﻿using lesson51.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lesson51.ViewModels
{
    public class PhoneViewModel
    {
        public IEnumerable<Phone> phones{ get; set; }

        public SelectList Phones { get; set; }

        public string Name { get; set; }
        public int Price { get; set; }

        public PageViewModel PageViewModel { get; set; }
    }
}
