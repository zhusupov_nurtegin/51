﻿using lesson51.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace lesson51.ViewModels
{
    public class UserViewModel
    {
        public IEnumerable<User> users { get; set; }

        public SelectList Companies { get; set; }

        public string Name { get; set; }

        public PageViewModel PageViewModel { get; set; }
    }
}
